module.exports = {
  configureWebpack: {
    devServer: {
        // https: true,
        host: 'front.rss.test',
        port: '3001',
        disableHostCheck: true,
    },
  },
  "transpileDependencies": [
    "vuetify"
  ],
}