import Vue from 'vue'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import axios from 'axios'

Vue.config.productionTip = false
Vue.prototype.$axios = axios
axios.defaults.baseURL = process.env.VUE_APP_AXIOS_BASE_URL
axios.defaults.withCredentials = true;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
