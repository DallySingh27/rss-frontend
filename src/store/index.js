import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
    async authUser() {
      const res = await axios({
      method: 'get',
      url: 'api/user',
      config: {}
      })
      .then(response => {
        if(response.status == 200) {
          localStorage.setItem('auth', true);
          return { status: true };
        }
        else {
          localStorage.setItem('auth', false);
          return { status: false };
        }
      })
      .catch(e => {
          localStorage.setItem('auth', false);
          return { status: false, message: e };
      });
      return res;
    }
  },
  modules: {
  }
})
